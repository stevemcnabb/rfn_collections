<?php

namespace Drupal\rfn_collections;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Path\PathValidatorInterface;
use Drupal\Core\Render\RendererInterface;
use Twig\Extension\AbstractExtension;
use Twig\Extension\ExtensionInterface;
use Twig\TwigFilter;
use Twig\TwigFunction;
use Twig\TwigTest;
use Drupal\rfn_collections\PlaylistBuilder;

/**
 * Twig extension.
 */
class RfnPlaylistTwigExtension extends AbstractExtension {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The path.validator service.
   *
   * @var \Drupal\Core\Path\PathValidatorInterface
   */
  protected $pathValidator;

  /**
   * The renderer.
   *
   * @var \Drupal\Core\Render\RendererInterface
   */
  protected $renderer;

  /**
   * The twig.extension service.
   *
   * @var \Twig\Extension\ExtensionInterface
   */
  protected $twigExtension;

  /**
   * Constructs a new RfnPlaylistTwigExtension object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Path\PathValidatorInterface $path_validator
   *   The path.validator service.
   * @param \Drupal\Core\Render\RendererInterface $renderer
   *   The renderer.
   * @param \Twig\Extension\ExtensionInterface $twig_extension
   *   The twig.extension service.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, PathValidatorInterface $path_validator, RendererInterface $renderer, ExtensionInterface $twig_extension) {
    $this->entityTypeManager = $entity_type_manager;
    $this->pathValidator = $path_validator;
    $this->renderer = $renderer;
    $this->twigExtension = $twig_extension;
  }

  /**
   * {@inheritdoc}
   */
  public function getFunctions() {
    return [
      new TwigFunction('generate_playlist_html', function ($tracks, $collection) {

        $playlist_builder = new PlaylistBuilder($tracks, $collection, $this->renderer);
        return $playlist_builder->build();
      }),
    ];
  }
}
