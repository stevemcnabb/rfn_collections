<?php

namespace Drupal\rfn_collections;

/**
 * Build a playlist given tracks and a collection.
 */
class PlaylistBuilder {

  public $tracks;

  public $collection;

  public $renderer;

  public function __construct($tracks, $collection, $renderer) {
    $this->tracks = $tracks;
    $this->collection = $collection;
    $this->renderer = $renderer;
  }

  public function build() {

    $output = '';

    // Twig templates:
    // - Wrapper
    // - Playlist row (selectable by javascript)
    // - Track details (title, album, artist if we are not in an album, cover if we are not in an album)
    //      + Duration
    //      + Track number

    $renderable = [
        '#type' => 'html',
        '#theme' => 'playlist',
        '#playlist_rendered' => $this->render_playlist_rows(),
    ];

    $output .= \Drupal::service('renderer')->renderPlain($renderable);

    return $output;
  }


  public function render_playlist_rows() {

    $output = '';

    foreach( $this->tracks as $track) {

      $renderable = [
        '#theme' => 'playlist_track',
        '#track_name' => $track->getTitle(),
        '#track_duration' => $track->get('field_duration')->value,
        '#track_streaming_uri' =>  $track->getStreamingUri(),
      ];

      $output .= $this->renderer->renderPlain($renderable);
    }

    return $output;
  }
}

