<?php

namespace Drupal\rfn_collections\Entity;

use Drupal\node\Entity\Node;

/**
 * Defines the collection entity.
 *
 * @ingroup collection
 *
 * @ContentEntityType(
 *   id = "collection",
 *   label = @Translation("collection"),
 *   base_table = "collection",
 *   revision_table = "collection_revision",
 *   revision_data_table = "collection_field_revision",
 *   entity_keys = {
 *     "id" = "id",
 *     "revision" = "revision_id",
 *     "published" = "status",
 *     "owner" = "uid",
 *   },
 *   revision_metadata_keys = {
 *     "revision_user" = "revision_user",
 *     "revision_created" = "revision_created",
 *     "revision_log_message" = "revision_log_message",
 *   },
 * )
 */
class Collection extends Node implements CollectionInterface {

  /**
   * Get tracks bound to this collection.
   */
  public function getTracks(): array {

    $tracks = [];
    $tracks_raw = $this->get('field_media_items')->getValue();

    foreach ($tracks_raw as $target_ids) {

      $track_nid = $target_ids['target_id'];
      $tracks[] = \Drupal::service('entity_type.manager')->getStorage('node')->load($track_nid);
    }

    return $tracks;
  }

  /**
   * Get artists bound to this collection.
   */
  public function getArtists(): array {

    $artists = [];
    $artists_raw = $this->get('field_artists')->getValue();

    foreach ($artists_raw as $target_ids) {

      $artist_nid = $target_ids['target_id'];
      $artists[] = \Drupal::service('entity_type.manager')->getStorage('node')->load($artist_nid);
    }

    return $artists;
  }

}
