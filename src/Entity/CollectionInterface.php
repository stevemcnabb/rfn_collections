<?php

namespace Drupal\rfn_collections\Entity;

use Drupal\node\NodeInterface;

/**
 * Collection interface.
 */
interface CollectionInterface extends NodeInterface {

  /**
   * Tracks associated with this collection.
   */
  public function getTracks():array;

  /**
   * Artist(s) associated with this collection.
   */
  public function getArtists():array;
}
